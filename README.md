# Como iniciar o projeto

Primeiro devemos instalar as dependências do projeto, e para isso devemos executar um dos seguintes comandos:
`yarn` ou `npm install`

Agora vamos criar o nosso arquivo de variáveis de ambiente. Temos que criar um arquivo com o nome `.env` 
na raiz do projeto e copiar os dados do arquivo `.env.example` para o nosso arquivo que acabamos de criar (.env).

Após criar o arquivo `.env` e copiar o dados a ele, devemos editar os valores das seguinte variaveis;
- **REACT_APP_URL_API**: deve ter o url de onde o projeto do backend está iniciado (ex: `http://localhost:3001`);
- **REACT_APP_REQUEST_TIMEOUT**: um valor numérico em milissegundos para a requisição ao servidor ser aguardada (ex: `5000`); 

Após as dependências serem instaladas podemos iniciar o projeto com o seguinte comando: `yarn start`

Com o projeto iniciado, podemos ir ao navegador da sua preferência e acessar o: `http://localhost:3000`

Agora é só usar :), lembrando que devemos estar com o backend (mcep-api) iniciado também.

Obs.: Esse projeto não possui testes :(
