import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  text: {
    color: theme.palette.text.primary
  }
}))
