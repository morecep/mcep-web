import React from 'react'
import { Typography } from '@material-ui/core'
import useStyles from './styles'
import clsx from 'clsx'
import { CommonProps } from '@material-ui/core/OverridableComponent'

interface Props {
  text: string
  className?: CommonProps<any>['className']
}

const Text = ({ text, className }: Props) => {
  const classes = useStyles()

  return (
    <Typography className={clsx(classes.text, className)}>{text}</Typography>
  )
}

export default Text
