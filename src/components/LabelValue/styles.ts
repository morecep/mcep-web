import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  mainContainer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 8,
    paddingBottom: 8,
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  labelText: {
    textAlign: 'right',
    flex: 1,
    color: theme.palette.text.secondary
  },
  valueText: {
    textAlign: 'left',
    color: theme.palette.text.primary,
    flex: 1,
    fontWeight: 'bold'
  }
}))
