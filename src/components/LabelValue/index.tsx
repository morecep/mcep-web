import React from 'react'
import { Typography } from '@material-ui/core'
import useStyles from './styles'

interface Props {
  label: string;
  value: string | number;
  spacing?: number;
}

const LabelValue = (props: Props) => {
  const classes = useStyles()

  return (
    <div className={classes.mainContainer}>
      <Typography className={classes.labelText}>{props.label}</Typography>
      <div style={{ width: props.spacing || 60 }}/>
      <Typography className={classes.valueText}>{props.value}</Typography>
    </div>
  )
}

export default LabelValue
