import AxiosService from './axiosService'
import { useDispatch } from 'react-redux'
import { setAddress, setMsgError } from '../redux/slices/systemSlice'

class AddressService {
  private dispatch = useDispatch()
  private api = AxiosService

  async getAddress (cep: string): Promise<void> {
    cep = cep.replace(/\D/g, '')
    console.log(cep)
    try {
      const res = await this.api.get(`/addresses/${cep}`)
      console.log(res.data)
      this.dispatch(setMsgError(undefined))
      this.dispatch(setAddress(res.data.body))
    } catch (e) {
      console.log(e)
      if (e) {
        this.dispatch(setAddress(undefined))
        this.dispatch(setMsgError(e.data.msg))
      } else {
        this.dispatch(setMsgError('Um erro desconhecido ocorreu'))
      }
    }
  }
}

export default AddressService
