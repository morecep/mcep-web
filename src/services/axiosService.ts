import axios from 'axios'

const AxiosService = axios.create({
  baseURL: process.env.REACT_APP_URL_API,
  timeout: process.env.REQUEST_TIMEOUT ? parseInt(process.env.REQUEST_TIMEOUT) : 8000
})

class CustomErrorAxios extends Error {
  private data = {
    msg: 'Um erro desconhecido ocorreu'
  }

  constructor (msg?: string) {
    super()
    if (msg) {
      this.data.msg = msg
    }
  }
}

AxiosService.interceptors.response.use((response) => {
  return response
}, (err) => {
  console.log(err, Object.keys(err), err.code)
  if (err.code) {
    switch (err.code) {
      case 'ECONNABORTED': {
        return Promise.reject(new CustomErrorAxios('Erro na comunicação com o servidor'))
      }
      default: {
        return Promise.reject(new CustomErrorAxios())
      }
    }
  }

  return Promise.reject(err.response)
})

export default AxiosService
