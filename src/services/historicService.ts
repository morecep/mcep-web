import { getLocalStorage, setLocalStorage } from '../utils/localStorageUtil'

class HistoricService {
  addNewCEP (cep: string): void {
    const listCep = getLocalStorage('cep')
    if (listCep) {
      if (!listCep.includes(cep)) {
        setLocalStorage('cep', [...listCep, cep])
      }
    } else {
      setLocalStorage('cep', [cep])
    }
  }

  getListCEP (): string[] {
    const listCep = getLocalStorage('cep')
    return listCep || []
  }

  removeCep (index: number): void {
    const listCep = getLocalStorage('cep')
    if (listCep) {
      listCep.splice(index, 1)
      setLocalStorage('cep', listCep)
    }
  }

  clearAll (): void {
    setLocalStorage('cep', [])
  }
}

export default HistoricService
