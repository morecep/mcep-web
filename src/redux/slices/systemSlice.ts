import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AddressData } from '../../../typings/address-typings'

interface SystemState {
  loading: boolean;
  address?: AddressData;
  errorMsg?: string;
  darkMode: boolean
}

const initialState: SystemState = {
  loading: false,
  address: undefined,
  errorMsg: undefined,
  darkMode: false
}

export const systemSlice = createSlice({
  name: 'system',
  initialState: initialState,
  reducers: {
    setLoading (state, action: PayloadAction<SystemState['loading']>) {
      state.loading = action.payload
    },
    setAddress (state, action: PayloadAction<SystemState['address']>) {
      state.address = action.payload
    },
    setMsgError (state, action: PayloadAction<SystemState['errorMsg']>) {
      state.errorMsg = action.payload
    },
    setDarkMode (state, action: PayloadAction<SystemState['darkMode']>) {
      state.darkMode = action.payload
    }
  }
})

// Action creators are generated for each case reducer function
export const {
  setLoading,
  setAddress,
  setMsgError,
  setDarkMode
} = systemSlice.actions

export default systemSlice.reducer
