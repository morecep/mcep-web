import { combineReducers, configureStore } from '@reduxjs/toolkit'
import systemReducer from './slices/systemSlice'

const rootReducer = combineReducers({
  system: systemReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default configureStore({
  reducer: rootReducer
})
