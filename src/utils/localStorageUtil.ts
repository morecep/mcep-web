
interface LocalStorageData {
  cep: string[];
  darkMode: boolean;
}

export const setLocalStorage = <T extends keyof LocalStorageData>(key: T, value: LocalStorageData[T]): void => {
  localStorage.setItem(key, JSON.stringify(value))
}

export const getLocalStorage = <T extends keyof LocalStorageData>(key: T): LocalStorageData[T] | null => {
  const value = localStorage.getItem(key)
  return value ? JSON.parse(value) : null
}
