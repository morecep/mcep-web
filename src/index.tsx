import React, { useState } from 'react'
import './index.css'
import ReactDOM from 'react-dom'
import HomePage from './pages/Home'
import { createMuiTheme, ThemeProvider } from '@material-ui/core'
import { Provider } from 'react-redux'
import store from './redux/store'

const App = () => {
  const [darkMode, setDarkMode] = useState(false)
  const theme = createMuiTheme({
    typography: {
      fontFamily: 'Nunito, sans-serif'
    },
    palette: {
      type: darkMode ? 'dark' : 'light',
      primary: {
        main: darkMode ? '#111315' : '#087CBD'
      },
      secondary: {
        main: darkMode ? '#737575' : '#0394b5'
      }
    }
  })

  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <React.StrictMode>
          <HomePage darkMode={setDarkMode}/>
        </React.StrictMode>,
      </Provider>
    </ThemeProvider>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
