import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(createStyles({
  title: {
    fontSize: 55,
    width: '100%',
    textAlign: 'center',
    color: '#F1F1F1',
    marginTop: 35,
    fontWeight: 'bold'
  },
  subtitle: {
    fontSize: 22,
    width: '100%',
    textAlign: 'center',
    color: '#F1F1F1',
    fontWeight: 'bold',
    padding: '0 5px'
  }
}))
