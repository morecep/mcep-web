import React from 'react'
import useStyles from './styles'
import Text from '../../../../components/Text'

const Title = () => {
  const classes = useStyles()
  return (
    <>
      <Text text={'MoreCEP'} className={classes.title}/>
      <Text text={'Encontre mais informações de endereço com seu cep'} className={classes.subtitle}/>
    </>
  )
}

export default Title
