import React from 'react'
import { Container } from '@material-ui/core'
import useStyles from './styles'
import { AddressData } from '../../../../../../../typings/address-typings'
import LabelValue from '../../../../../../components/LabelValue'

interface Props {
  address: AddressData
}

const HeaderAddress = ({ address }: Props) => {
  const classes = useStyles()

  return (
    <Container className={classes.headerContainer}>
      {Object.entries(address).map((item, index) => {
        return (
          <LabelValue key={index} label={item[0]} value={item[1].length > 0 ? item[1] : '-'} />
        )
      })}
    </Container>
  )
}

export default HeaderAddress
