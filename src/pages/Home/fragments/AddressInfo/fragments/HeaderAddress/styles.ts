import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  headerContainer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    paddingTop: 5
  }
}))
