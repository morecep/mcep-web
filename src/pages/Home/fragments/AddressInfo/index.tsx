import React from 'react'
import { CircularProgress, Container } from '@material-ui/core'
import useStyles from './styles'
import { useSelector } from 'react-redux'
import { RootState } from '../../../../redux/store'
import HeaderAddress from './fragments/HeaderAddress'
import Text from '../../../../components/Text'

const AddressInfo = () => {
  const classes = useStyles()
  const { address, errorMsg, loading } = useSelector((state: RootState) => state.system)

  return (
    <Container className={classes.addressContainer}>
      {loading ? (
       <CircularProgress size={25} color={'secondary'}/>
      ) : address ? (
        <HeaderAddress address={address} />
      ) : errorMsg ? (
        <Text text={errorMsg}/>
      ) : (
        <Text text={'Digite um cep acima'}/>
      )}
    </Container>
  )
}

export default AddressInfo
