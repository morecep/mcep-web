import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  addressContainer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 8,
    marginTop: 40,
    width: '80%',
    background: theme.palette.background.default,
    borderRadius: 8,
    marginBottom: 40,
    [theme.breakpoints.down('sm')]: {
      width: '90%'
    }
  }
}))
