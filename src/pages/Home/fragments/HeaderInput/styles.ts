import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  mainContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 80
  },
  formContainer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column'
    }
  },
  containerHistoric: {
    display: 'flex',
    justifyContent: 'center'
  },
  containerTextInput: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: 250,
    padding: '8px 12px',
    background: theme.palette.background.default,
    [theme.breakpoints.down('sm')]: {
      width: '90%',
      marginBottom: 10
    }
  },
  divider: {
    height: 30,
    margin: 8
  },
  iconButton: {
    padding: 10,
    color: theme.palette.secondary.main
  },
  textInput: {
    color: theme.palette.text.primary,
    flex: 1,
    fontWeight: 'bold'
  },
  btnFavorite: {
    height: '100%',
    marginLeft: 10,
    textTransform: 'none'
  },
  btnFavoriteText: {
    color: '#564F0B',
    fontWeight: 'bold'
  }
}))
