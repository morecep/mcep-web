import React, { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react'
import {
  CircularProgress,
  Container,
  Divider, FormControl, FormHelperText,
  IconButton, InputAdornment,
  InputBase,
  Paper
} from '@material-ui/core'
import { Close, Search } from '@material-ui/icons'
import useStyles from './styles'
import AddressService from '../../../../services/addressService'
import { useDispatch, useSelector } from 'react-redux'
import { setLoading } from '../../../../redux/slices/systemSlice'
import { RootState } from '../../../../redux/store'
import HistoricService from '../../../../services/historicService'
import HistoricBox from './fragments/HistoricBox'

const HeaderInput = () => {
  const classes = useStyles()
  const dispatch = useDispatch()

  const addressSrv = new AddressService()
  const historicSrv = new HistoricService()

  const btnRef = useRef<HTMLButtonElement>(null)
  const [cep, setCEP] = useState('')
  const [canSubmit, setCanSubmit] = useState(true)
  const [onFocus, setOnFocus] = useState(false)
  const [onMouseOverHistoric, setOnMouseOverHistoric] = useState(false)
  const { loading } = useSelector((state: RootState) => state.system)

  const onSetCEP = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    e.currentTarget.maxLength = 9
    let value = e.currentTarget.value
    value = value.replace(/\D/g, '')
    value = value.replace(/^(\d{5})(\d)/g, '$1-$2')
    setCEP(value)
  }, [])

  useEffect(() => {
    setCanSubmit(true)
  }, [cep])

  const onInputFocus = (state: boolean) => {
    setOnFocus(state || onMouseOverHistoric)
  }

  const onSubmit = async () => {
    if (cep.length === 9 && !loading) {
      if (btnRef.current) {
        btnRef.current.focus()
      }
      setOnFocus(false)
      historicSrv.addNewCEP(cep)
      dispatch(setLoading(true))
      await addressSrv.getAddress(cep)
      dispatch(setLoading(false))
    } else {
      setCanSubmit(false)
    }
  }

  const onKeyDown = useCallback(async (key: string) => {
    if (key === 'Enter') {
      await onSubmit()
    }
  }, [cep])

  const onSelectHistoric = (cep: string) => {
    setOnFocus(false)
    setCEP(cep)
  }

  return (
    <Container className={classes.mainContainer}>
      <Container className={classes.formContainer}>
        <Paper className={classes.containerTextInput}>
          <FormControl className={classes.textInput} error={!canSubmit}>
            <InputBase
              onFocus={() => onInputFocus(true)}
              onBlur={() => onInputFocus(false)}
              value={cep}
              onChange={onSetCEP}
              onKeyDown={(ev) => onKeyDown(ev.key)}
              placeholder={'Digite um cep'}
              endAdornment={
                <InputAdornment position={'end'} >
                  {cep.length > 0 &&
                    <IconButton size={'small'} onClick={() => setCEP('')}>
                      <Close fontSize={'small'}/>
                    </IconButton>
                  }
                </InputAdornment>
              }
            />
            {!canSubmit && <FormHelperText>Digite um cep válido</FormHelperText>}
          </FormControl>
          <Divider className={classes.divider} orientation={'vertical'}/>
          <IconButton className={classes.iconButton} onClick={() => onSubmit()} disabled={loading} ref={btnRef}>
            {loading ? (
              <CircularProgress size={25} color={'secondary'}/>
            ) : (
              <Search />
            )}
          </IconButton>
        </Paper>
      </Container>
      <Container className={classes.containerHistoric}>
        {onFocus && <HistoricBox onSelect={onSelectHistoric} onMouseInside={setOnMouseOverHistoric} />}
      </Container>
    </Container>
  )
}

export default HeaderInput
