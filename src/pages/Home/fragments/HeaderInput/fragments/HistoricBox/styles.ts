import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  mainContainer: {
    position: 'absolute',
    marginTop: 10,
    width: '50%',
    background: theme.palette.background.default,
    borderRadius: 5,
    padding: 8,
    maxHeight: 120,
    overflowY: 'auto',
    zIndex: 9,
    border: `solid 1px ${theme.palette.divider}`,
    [theme.breakpoints.down('sm')]: {
      width: '90%'
    }
  },
  containerGrid: {
    display: 'flex',
    justifyContent: 'space-around',
    flexWrap: 'wrap'
  },
  containerGridItem: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    color: theme.palette.text.primary,
    fontWeight: 'bold'
  },
  btnClearHistoric: {
    width: '100%',
    marginTop: 10,
    textTransform: 'none'
  }
}))
