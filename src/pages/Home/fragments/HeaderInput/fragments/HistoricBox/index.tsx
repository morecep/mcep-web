import React, { useEffect, useState } from 'react'
import { Button, Container, Grid, IconButton } from '@material-ui/core'
import useStyles from './styles'
import HistoricService from '../../../../../../services/historicService'
import { Close } from '@material-ui/icons'
import { useSelector } from 'react-redux'
import { RootState } from '../../../../../../redux/store'
import Text from '../../../../../../components/Text'

interface Props {
  onSelect(value: string): void
  onMouseInside(state: boolean): void
}

const HistoricBox = (props: Props) => {
  const classes = useStyles()
  const { loading } = useSelector((state: RootState) => state.system)

  const historicSrv = new HistoricService()

  const [listHistoric, setListHistoric] = useState<string[]>()

  const getHistoric = () => {
    const list = historicSrv.getListCEP()
    if (list.length) {
      setListHistoric(list)
    } else {
      setListHistoric(undefined)
    }
  }

  useEffect(() => {
    getHistoric()
  }, [loading])

  const removeHistoric = (index: number) => {
    historicSrv.removeCep(index)
    getHistoric()
  }

  const clearAllHistoric = () => {
    historicSrv.clearAll()
    getHistoric()
    props.onMouseInside(false)
  }

  return (
    <Container
      className={classes.mainContainer}
      onMouseEnter={() => props.onMouseInside(true)}
      onMouseLeave={() => props.onMouseInside(false)}
      onTouchEnd={() => props.onMouseInside(false)}
    >
      <Grid className={classes.containerGrid}>
        {listHistoric ? listHistoric.map((hist, index) => {
          return (
            <Grid item xs={5} key={index} className={classes.containerGridItem}>
              <Button className={classes.text} onClick={() => props.onSelect(hist)}>{hist}</Button>
              <IconButton size={'small'} onClick={() => removeHistoric(index)}>
                <Close fontSize={'small'}/>
              </IconButton>
            </Grid>
          )
        }) : (
          <Text text={'Não possui histórico'}/>
        )}
        {listHistoric && (
          <Button className={classes.btnClearHistoric} onClick={() => clearAllHistoric()}>
            <Text text={'Limpar Historico'}/>
          </Button>
        )}
      </Grid>
    </Container>
  )
}

export default HistoricBox
