import React, { useEffect } from 'react'
import useStyles from './styles'
import { Container, FormControlLabel, Switch } from '@material-ui/core'
import HeaderInput from './fragments/HeaderInput'
import AddressInfo from './fragments/AddressInfo'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/store'
import { setDarkMode } from '../../redux/slices/systemSlice'
import Text from '../../components/Text'
import { getLocalStorage, setLocalStorage } from '../../utils/localStorageUtil'
import Title from './fragments/Title'

interface Props {
  darkMode(enabled: boolean): void
}

const HomePage = (props: Props) => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { darkMode } = useSelector((state: RootState) => state.system)

  useEffect(() => {
    const darkMode = getLocalStorage('darkMode')
    if (darkMode) {
      dispatch(setDarkMode(darkMode))
    }
  }, [])

  useEffect(() => {
    props.darkMode(darkMode)
    setLocalStorage('darkMode', darkMode)
  }, [darkMode])

  const handleDarkMode = () => {
    dispatch(setDarkMode(!darkMode))
  }

  return (
    <Container disableGutters maxWidth={false} className={classes.mainContainer}>
      <FormControlLabel
        control={
          <Switch
            color={'default'}
            checked={darkMode}
            onChange={handleDarkMode}
          />
        }
        label={<Text text={`Ativar ${darkMode ? 'lightMode' : 'darkMode'}`} />}
      />
      <Title />
      <HeaderInput />
      <AddressInfo />
    </Container>
  )
}

export default HomePage
