import { createStyles, makeStyles } from '@material-ui/core'

export default makeStyles(theme => createStyles({
  mainContainer: {
    width: '100vw',
    height: '100vh',
    position: 'fixed',
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    overflowY: 'auto'
  },
  title: {
    fontSize: 55,
    width: '100%',
    textAlign: 'center',
    color: '#F1F1F1',
    marginTop: 35,
    fontWeight: 'bold'
  },
  formContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 80
  },
  containerTextInput: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: 200,
    padding: '8px 12px'
  },
  divider: {
    height: 30,
    margin: 8
  },
  iconButton: {
    padding: 10
  },
  textInput: {
    fontFamily: theme.typography.fontFamily,
    flex: 1,
    fontWeight: 'bold'
  },
  btnFavorite: {
    backgroundColor: '#DADE00',
    height: '100%',
    marginLeft: 10,
    textTransform: 'none'
  },
  btnFavoriteText: {
    color: '#564F0B',
    fontWeight: 'bold'
  }
}))
